:BasicUpstart2(main)

.const SCREEN_MEMORY = $0400

.var hello = "hello"
.var world = "world"
.var space = ' '

.var hello_world = hello +space + world + 2

.print hello_world
.print 'x'
.print 53280

.print ""
.print "Integers"
.print toIntString($24)
.print toIntString($d021)
.print toIntString(%10110101)

.print ""
.print "Binary"
.print toBinaryString(220)
.print toBinaryString(35)

.print ""
.print "Hex"
.print toHexString(53280)



.pc = * "Main"
main:

	rts

.pc = * "Data"