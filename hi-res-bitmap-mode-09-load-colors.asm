:BasicUpstart2(main)

.label screen_control_register = $d011
.label memory_setup_register = $d018
.label border_color = $d020

main:
  lda screen_control_register
  ora #%00100000
  sta screen_control_register

  lda #DARK_GRAY
  sta border_color

  :choose_screen_memory(3)

  :choose_bitmap_memory(1)

  jmp * 

.pc = screen_memory(3)
.import binary "colors.bin"

.pc = bitmap_memory(1) 
.import binary "hires-bitmap.bin"

.macro choose_bitmap_memory(index) {
  lda memory_setup_register
  and #%11110111
  ora #[index << 3]
  sta memory_setup_register
}

.function bitmap_memory(index) {
  .return 1024*8*index
}

.macro choose_screen_memory(index) {
  lda memory_setup_register
  and #%00001111
  ora #[index << 4]
  sta memory_setup_register
}

.function screen_memory(index) {
  .return 1024*index
}



