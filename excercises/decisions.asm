:BasicUpstart2(main)

.const BORDER = $d020
.const BACKGROUND = $d021

.const MIN_SHIELD_DAMAGE = -128
.const MAX_SHIELD_DAMAGE = 127

.const MAX_HEALTH = 127
.const MIN_HEALTH = -128

.const MONSTER_ATTACK_DAMAGE = 90

main:

shield_takes_damage:
  clc
  lda shield_damage
  adc #MONSTER_ATTACK_DAMAGE
  sta shield_damage
  bvs shield_destroyed
  bpl shield_almost_destroyed

shield_still_works:
  lda #GREEN
  sta BORDER
  lda #GREEN
  sta BACKGROUND
  jmp end

shield_almost_destroyed:
  lda #YELLOW
  sta BORDER
  lda #GREEN
  sta BACKGROUND
  jmp end

shield_destroyed:
  lda #RED
  sta BORDER
  lda #MAX_SHIELD_DAMAGE
  sta shield_damage
  jmp player_takes_damage


player_takes_damage:
  sec
  lda player_health
  sbc #MONSTER_ATTACK_DAMAGE
  sta player_health
  bvs player_died
  bmi player_almost_died

player_still_lives:
  lda #GREEN
  sta BACKGROUND
  jmp end

player_almost_died:
  lda #YELLOW
  sta BACKGROUND
  jmp end

player_died:
  lda #RED
  sta BACKGROUND
  lda #MIN_HEALTH
  sta player_health
  jmp end

end:
  rts

.pc = 2500
shield_damage:
  .byte MIN_SHIELD_DAMAGE
player_health:
  .byte MAX_HEALTH

