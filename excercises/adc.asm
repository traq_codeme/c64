:BasicUpstart2(main)

main:
	clc
	lda score
	adc monster_kill_exp
	sta score
	lda score  + 1
	adc monster_kill_exp + 1
	sta score + 1
	rts

.pc = 2500
score:
	.word $1234
monster_kill_exp:
	.word $01ff
score_after_killing_monster:
	.word $1234 + $01ff