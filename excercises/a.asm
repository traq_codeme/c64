:BasicUpstart2(main)

.const TOP_LEFT_CHARACTER = $0400
.const BORDER = $d020

main:
  lda lives
  beq game_over

  dec lives
  bne still_playing

game_over:
  lda #BLACK
  sta BORDER
  jmp print_lives

still_playing:
  lda #GREEN
  sta BORDER
  jmp print_lives

print_lives:
  lda #'0'
  clc
  adc lives
  sta TOP_LEFT_CHARACTER
  rts

.pc = 2500
lives:
  .byte 3