:BasicUpstart2(main)

.label color_memory = $d800
.label screen_control_register = $d011
.label screen_control_register2 = $d016
.label memory_setup_register = $d018
.label border_color = $d020
.label background_color = $d021

main:
  lda screen_control_register
  ora #%00100000
  sta screen_control_register

  lda screen_control_register2
  ora #%00010000
  sta screen_control_register2

  lda #BLACK
  sta border_color
  sta background_color

  :choose_screen_memory(3)

  :fill_memory(LIGHT_GRAY, color_memory, 1000)
  
  :choose_bitmap_memory(1)

  jmp * 

.pc = screen_memory(3)
  .fill 1000, DARK_GRAY << 4 | GRAY

.pc = bitmap_memory(1) 
  .import binary "test_c64_sprites.bin"

.function bitmap_memory(index) {
  .return 1024*8*index
}

.macro choose_bitmap_memory(index) {
  lda memory_setup_register
  and #%11110111
  ora #[index << 3]
  sta memory_setup_register
}

.macro choose_screen_memory(index) {
  lda memory_setup_register
  and #%00001111
  ora #[index << 4]
  sta memory_setup_register
}

.function screen_memory(index) {
  .return 1024*index
}

.macro fill_memory(char, start, count) {
  .var unroll_count = count / 250
  ldx #250
  lda #char
loopx:
  .for (var i = 0; i < unroll_count; i++) {
    sta start + 250*i - 1, X
  }
  dex
  bne loopx
}
