.importonce
anim_hole1:
  .for (var i = 0; i < 1; i++) {
      ldx current_coords + i
      lda x_coordinates_osemka, X
      sta sprites.positions + 2*i + 0
      lda y_coordinates_osemka, X
      sta sprites.positions + 2*i + 1
      inx
      cpx #COORDINATES_COUNT
      bne end
      ldx #0 
    end:
      stx current_coords + i
    }
    rts

  anim_hole2:
  .for (var i = 0; i < 1; i++) {
      ldx current_coords + i
      lda x_coordinates_parabola_down, X
      sta sprites.positions + 2*i + 0
      lda y_coordinates_osemka, X
      sta sprites.positions + 2*i + 1
      inx
      cpx #COORDINATES_COUNT
      bne end
      ldx #0 
    end:
      stx current_coords + i
    }
    rts

 anim_hole3:
  .for (var i = 0; i < 1; i++) {
      ldx current_coords + i
      lda x_coordinates_circle, X
      sta sprites.positions + 2*i + 0
      lda y_coordinates_osemka, X
      sta sprites.positions + 2*i + 1
      inx
      cpx #COORDINATES_COUNT
      bne end
      ldx #0 
    end:
      stx current_coords + i
    }
    rts

 anim_hole4:
  .for (var i = 0; i < 1; i++) {
      ldx current_coords + i
      lda x_coordinates_test, X
      sta sprites.positions + 2*i + 0
      lda y_coordinates_osemka, X
      sta sprites.positions + 2*i + 1
      inx
      cpx #COORDINATES_COUNT
      bne end
      ldx #0 
    end:
      stx current_coords + i
    }
    rts

current_coords:
  .fill 8, 10*[7-i] 

//eight
x_coordinates_osemka:
  .fill COORDINATES_COUNT, 150+80*sin(toRadians(i*8*360/COORDINATES_COUNT))
y_coordinates_osemka:
  .fill COORDINATES_COUNT, 130-50*cos(toRadians(i*4*360/COORDINATES_COUNT)) 

//parabola  down
x_coordinates_parabola_down:
  .fill COORDINATES_COUNT, 150+80*sin(toRadians(i*2*360/COORDINATES_COUNT))

//circle
x_coordinates_circle:
  .fill COORDINATES_COUNT, 150+80*sin(toRadians(i*4*360/COORDINATES_COUNT))

//test
x_coordinates_test:
  .fill COORDINATES_COUNT, 150+80*sin(toRadians(i*1*360/COORDINATES_COUNT))
y_coordinates_test:
  .fill COORDINATES_COUNT, 130-50*cos(toRadians(i*1*360/COORDINATES_COUNT)) 
