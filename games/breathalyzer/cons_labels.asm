.importonce

//joystick zmienne
.label joystick1_port = $dc01
.label joystick2_port = $dc00
.label joyport = $dc00

.const COLLISIONDET = %00000001
.const JOY_UP     = %00000001
.const JOY_DOWN   = %00000010
.const JOY_LEFT   = %00000100
.const JOY_RIGHT  = %00001000
.const JOY_BUTTON = %00010000
.const JOY_NEUTRAL = %00011111

.const COORDINATES_COUNT = 256 // zmienna do zabawy w ruch sprita (sinus i cosins)
.const VIC2 = $d000 //początek pamięci VIC2 i od razu  x coordinate dla sprite 0

.label screen_memory = $0c00 + $8000
.label border_color_reg = $d020
.label background_color_reg = $d021
.label ext1_background_color_reg = $d021 +1
.label irq_handler_vector = $0314 // rejestr przerywan

.namespace sprites {
  .label positions = VIC2
  .label enable_bits = VIC2 + 21 // rejestr włączania spritów
  .label pointers = screen_memory + 1024 - 8  // sprite pointers - k#wa nastepny fragment pamięci/rejestr żeby wyświetlić dane sprite z plik z kilkoma spritami
  .label vertical_stretch_bits = VIC2 + 23 //rejestr powiekszenia spritów w pionie
  .label horizontal_stretch_bits = VIC2 + 29 //rejestr powiekszenia spritów w poziomie
  .label colors = VIC2 + 39 //rejestr kolorów spritów
  .label collision = VIC2 + 30 // rejestr kolizji
  .label multicolor_bits = VIC2 + 28 //
  .label dziurka_x = sprites.positions +0
  .label dziurka_y = sprites.positions +1
  .label klucz_x = sprites.positions +2
  .label klucz_y = sprites.positions +3
}
