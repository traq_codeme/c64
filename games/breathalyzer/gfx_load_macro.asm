.importonce

.macro gfx_load(){
      lda $dd00       
      and #$fc  //zeruje dwa najmłodsze bity
      ora #1        
      sta $dd00
      //powższe cztery linie przenosi pamięc VIC II w trzecią ćwiartkę od 8000 do BFFF
      lda #%00111000
      sta $d018
      lda #$d8
      sta $d016
      lda #$3b
      sta $d011
      lda #0
      sta $d020
      lda #picture.getBackgroundColor()
      sta $d021
      ldx #0
    :copy_pages(image_screen, $0c00 + $8000, 4)
    :copy_pages(image_color, $d800, 4)
    :copy_pages(image_bitmap, $2000 + $8000, 32)
    } 

