.import source "macros.asm"
//.import source "helpers.asm"
.import source "binimports.asm" //binary files import
.import source "cons_labels.asm" //.cons and .labels 
.import source "gfx_load_macro.asm" //macro for start gfx load
.import source "collisions.asm"


:BasicUpstart2(main)
//values
// last state of joystick
joylast: .byte 0
//score
score: 
  .byte 0
//tries
tries: 
  .byte 9

//texts
score_txt: 
  .text "SCORE"
  .byte 0
tries_txt: 
  .text "TRIES"
  .byte 0

catch_hole: 
  .text " Try key to the hole, and fire! "
  .byte 0

end_txt: 
  .text "  You woke up wife, run away!  "
  .byte 0

win_txt:
  .text "YOUN WIN! Are you sober? Shame!"
  .byte 0

.pc = * "Main"
main:
    
// grapic load procedure
  gfx_load:
    :gfx_load()
    :music_init(0)
    sei // disable irq
    lda #<only_music
    sta irq_handler_vector //$0314
    lda #>only_music
    sta irq_handler_vector + 1 //$0315

    //magia z przerywaniami. 
    lda #$7         // 0---00111
    sta $dc0d       // disable timer interrupts
    lda #$81        // 10000001
    sta $d01a       // enable rater interrupts
    lda #$80        // dec 128
    sta $d012       // Raster line to generate interrupt at  (bits #0-#7).
    cli // enable irq
    //fire button check
    fire_check:
      :joy(0, 4) 
      beq !end+ 
      jmp fire_check
  !end:
  :clear_screen()
  :score_display()
  :write_text(catch_hole,screen_memory +0,0)
  :sprites_preaparation()
  :music_init(0)

  // działanie na przerywaniu: muzyka i animacja
  sei // disable irq
  lda #<animation_and_music
  sta irq_handler_vector //$0314
  lda #>animation_and_music
  sta irq_handler_vector + 1 //$0315

  //magia z przerywaniami. 
  lda #$7         // 0---00111
  sta $dc0d       // disable timer interrupts
  lda #$81        // 10000001
  sta $d01a       // enable rater interrupts
  lda #$5b        // 0 <- MSB to Write: Raster line to generate interrupt at (bit #8). and rest 0011 011
  sta $d011       // screen control register
  lda #$80        // dec 128
  sta $d012       // Raster line to generate interrupt at  (bits #0-#7).
  lda #%11001000
  sta $d016
  cli // enable irq
  
mainloop:
  jmp mainloop //endless loop


//##### Main label ###########
.pc = $4000 "Irq"
animation_and_music:
  inc border_color_reg //blinking border effect
  
  :handle_joystick_input()    
  :animacje_dziurki()

only_music:
  //:music_play()

end_of_irq:
//reszta przerywania
    lda #1
    sta $d019
    pla // get accumylator from the stack
    tay // transfer accumulator to Y
    pla // get accumylator from the stack
    tax // transfer accumulator to X
    pla // get accumylator from the stack
    rti // return from iterrupt

//holes animations
.import source "animations.asm"
