# Add and Carry On

In a variety of games we need to track various numbers. Score, number of lives, amount of fuel, experience points, or even the position of the player are just few examples. Therefore it's a good idea to learn commands that allow us to modify these numbers.

Until now, we've learned how to change a number by one using either X or Y registers and their inx, iny, dex, dey commands. X and Y registers are most commonly used to index various things and you will commonly see them being decremented or incremented.

For the sake of completeness let's also learn the inc and dec commands. They allow us to directly increment and decrement the value in the memory, without the need of putting it into a register.   

If we need to add two arbitrary numbers we can use the ADC command. ADC stand for Add with Carry and it can add an accumulator register to a number or contents of the memory and store the result back in the accumulator. As the name suggests it also takes the carry flag into account.



But wait, what the hell is the carry flag?

Let's try to explain that by example. When we add two decimal numbers together we align them and add each digit from right to left. When the sum exceeds or equals 10 a one is carried to another column and is added to the result.


```
 1   // Carry
 76
 59
---
135
```

If we add two binary numbers together, we can use exactly the same algorithm. If the sum exceeds or equals 2 a one is carried to the next column. 
```
 111        // Carry
0100 1100
0011 1011
---------
1000 0111
```

When we add multi-byte numbers together, on commodore 64 we have no other way but to do it byte by byte.

First we add the less significant byte, if the result exceeds 255, we get a 1 to carry to the more significant byte addition. And this is where the carry flag jumps in. It is used literally to carry the 1 between additions.

The ADC command can be defined by this formula:

```
A = A + M + C // where the m is the memory and the C is the carry flag.
```

So if the carry is set the result will be incremented by one.

During the execution of our program the carry flag can be set to one by some of our previous operations. To ensure we add numbers properly we need to clear the carry flag before the addition of the least significant byte. We can do that with the CLC (or Clear Carry) command.

Let's see few examples.

In this program we will add two values together and we will store the result in the memory.

First let's reserve a memory for the result, place it at the address 2500 and give it a name.
Then we will use a .byte directive to put a value 0 at this place in the memory.

We have just effectively defined a one byte variable.

Now the actual program.

First we clear the carry flag.

Then we load 12 into the accumulator, add 15 to it, and store the result.

The important thing to notice here is that the carry flag is not modified by the lda command. This is the only reason that LDA can be safely put between the CLC and the first addition.

``` asm
main:
  clc
  lda #12
  adc #15
  sta result
  rts

.pc = 2500
result:
  .byte 0
```

After our program ends. We can use the peek command to check the computation.

``` basic
peek 2500
```

Similarly we can use the adc command to add values in the memory.

Here we define two variables, the experience of the player, and the monster kill experience.
The player have just killed the monster. We want to update her experience. So again, we clear the carry flag. 
Load the experience to the accumulator. Add the monster kill experience. And store the result back.

``` asm
main:
  clc
  lda experience 
  adc monster_kill_exp
  sta experience
  rts

.pc = 2500
experience:
  .byte 100
monster_kill_exp:
  .byte 10
```

And once again we can use the peek command to check the result.

Now imagine we need to add larger numbers.

The player has a score, which is a 16-bit number. We can define a variable like that using the .word directive.

So let's define a score variable and set it to a hexadecimal number $1234.

There is also a variable representing a score awarded for killing a monster.

Let's also define a temporary variable that we will use to check the correctness of the result. So let's just add those numbers at the compile time and store the result.

Before we start writing our program let's compile what we have and see how the numbers are laid out in the memory.

``` asm
main:
  rts

.pc = 2500
score:
  .word $1234

monster_kill_score:
  .word $01ff

score_after_killing_monster:
  .word $1234 + $01ff

```


With the monitor's m command we can see that on Commodore 64 the little endian byte order is used for 16 bit numbers. It means that the least significant byte is stored first in the memory.

We can also see that our numbers are summed up correctly.

```
m +2500 +2505
```

Now to add the 16 bit number we simply clear the carry at the beginning and repeat the add operation for each byte starting from the least significant one.
 
``` asm
main:
  clc
  lda score 
  adc monster_kill_score
  sta score
  lda score + 1
  adc monster_kill_score + 1
  sta score + 1
  rts
```

This algorithm will work for any multibyte number regardless of how many bytes are needed to store it.

In the next episode we'll see how to subtract numbers in assembly and how to deal with negative numbers. On top of that we'll find out how designers of the 6502 and 6510 processors were able to make the subtraction efficient.

See you soon!
