.import source "helpers.asm"

:BasicUpstart2(main)
.label kernal_chrout = $ffd2
.label screen_memory = $0400

.label joystick1_port = $dc01
.label joystick2_port = $dc00

.const JOY_UP     = %00000001
.const JOY_DOWN   = %00000010
.const JOY_LEFT   = %00000100
.const JOY_RIGHT  = %00001000
.const JOY_BUTTON = %00010000

.const JOY_ANY = %00011111

main:
  lda #147
  jsr kernal_chrout

game_loop:
  :draw_player()
  :handle_joystick_input(joystick1_port)
  jmp game_loop
  
.pc = * "Data"
  player_character: .byte 'p'
  player_position: .word screen_memory + 500

.macro handle_joystick_input(joystick) {
  lda joystick 
  and #JOY_BUTTON
  bne !no_action+
!action:
  :mov #'a'; player_character
  jmp end
!no_action:
  lda joystick 
  and #JOY_UP
  bne !no_action+
!action:
  :mov #'u'; player_character
  jmp end
!no_action:
  lda joystick 
  and #JOY_DOWN
  bne !no_action+
!action:
  :mov #'d'; player_character
  jmp end
!no_action:
  lda joystick 
  and #JOY_LEFT
  bne !no_action+
!action:
  :mov #'l'; player_character
  jmp end
!no_action:
  lda joystick 
  and #JOY_RIGHT
  bne !no_action+
!action:
  :mov #'r'; player_character
  jmp end
!no_action:
end:
}
.macro draw_player() {
  :mov16 player_position; position
  lda player_character
.label position = * + 1
  sta screen_memory + 500
}


