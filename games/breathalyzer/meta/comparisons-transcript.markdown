# Comparisons

In the previous episode, we've been using C, V, and N flags along with their corresponding branching instructions to check for overflows and the sign of a number. There is also a Z flag that is set when number equals 0 and is cleared otherwise. It also has corresponding branching instructions - BEQ, that is Branch if Equal and BNE or Branch if Not Equal.

The Z flag is useful when we want to make exact comparisons.

Let's see an example. 

In this program, we simulate a game where a player starts with some number of lives and is just about to loose one.

``` asm
:BasicUpstart2(main)

main:
  rts

.pc = 2500
lives:
  .byte 3
```

If we assume that the number of lives will always be a one-digit positive number, we can use a simple trick to print the number of lives by simply adding it to the screen code of the zero character.
 
``` asm
print_lives:
  lda #'0'
  clc
  adc lives
  sta TOP_LEFT_CHARACTER
```

We will have two states represented by the color of the border:
When the game is over it will be black. And if we are still playing the game, the border will be green.

Now in the main part of our program we'll load the lives variable into the accumulator. It changes the Z flag depending on the value. If we have no lives left, the Z flag will be set, and the beq instruction will jump into the game_over state.

``` asm
  lda lives
  beq game_over
```

On the other hand, if the Z flag was cleared, we will decrement the number of lives and check again. In this case, if the number of lives is not 0 the bne instruction will jump into the still_playing state.

If we run this program, we can see that it correctly detects when we ran out of lives and stays in the game_over state afterward.

The N and Z flags combined are especially powerful when we need to compare numbers. First of all they are changed whenever we load a new value into a register, so we can quickly determine if the value is negative, positive or equal to zero.

To experiment with that let's create another simple program.

We'll need a variable, let's call it a number_a. It will hold a value that will be compared with 0.

``` asm
.pc = 2500
number_a:
  .byte  -5
```

Then we'll print a character 'a' and '0' with some space in between, in the top-left corner of the screen.

``` asm
.const TOP_LEFT_CHARACTER = $0400

main:
  lda #'a'
  sta TOP_LEFT_CHARACTER + 0
  
  lda #' '
  sta TOP_LEFT_CHARACTER + 1
  sta TOP_LEFT_CHARACTER + 2
  sta TOP_LEFT_CHARACTER + 3

  lda #'0'
  sta TOP_LEFT_CHARACTER + 4

```

Now, we'll introduce three possible outcomes that will print appropriate relations between the number_a and 0.

``` asm
 
a_less_than_0:
  lda #'<'
  sta TOP_LEFT_CHARACTER + 2
  jmp end

a_equal_to_0:
  lda #'='
  sta TOP_LEFT_CHARACTER + 2
  jmp end

a_greater_than_0:
  lda #'>'
  sta TOP_LEFT_CHARACTER + 2
  jmp end

end:
  rts
```

Finally, we'll load the value of the number_a variable into the accumulator that sets both N and Z flags.

The last thing we need to do is to use branching instructions to choose the appropriate outcome.

There are multiple ways to do that, but some are better than the other.

For example, if we start with the bpl instruction we will jump whenever a is greater or equal to 0 but this forces us to introduce an intermediate state.

``` asm
  bpl a_greater_or_equal_to_0
  jmp a_less_than_0
a_greater_or_equal_to_0:
  beq a_equal_to_0
  jmp a_greater_than_0
```

We find ourselves in a similar situation if we make our first check using the bne command.

``` asm
  bne a_not_equal_to_0
  jmp a_equal_to_0
a_not_equal_to_0:
  bmi a_less_than_0
  jmp a_greater_than_0
```

By asking the right questions, we can simplify our program.

For example, if we start with the beq command we can get rid of an unnecessary intermediate state.

``` asm
  beq a_equal_to_0
  bmi a_less_than_0
  jmp a_greater_than_0
```

If we are brave enough, we can even take advantage of the order of execution of the rest of our program and optimize the code even further. This is fine, but it will force us to be extra careful whenever we need to modify this part of the program.

``` asm
  beq a_equal_to_0
  bpl a_greater_than_0
``` 

Great, it works. But we already knew how to compare numbers with 0. The interesting question is - how can we use tools we already know to compare two arbitrary numbers together.

Let's assume that the a is less than b. This means that a - b is less than 0. And this works with other relations as well.

So if we introduced a number_b to our program and subtracted it from the number_a, the accumulator would hold a result. And it would seem that the Z and N flags would represent how the result relates to zero.

``` asm
  lda number_a
  sec
  sbc number_b
```

In other words, we would get the answer to the question "How those two arbitrary numbers relate to each other?" 

When we run the program it seems that this method works. Unfortunately it's not as simple as that. 
For example, if we try to compare 1 with 255 or 127 and -128 we'll get incorrect results.

Unfortunately the subtraction method works only with unsigned numbers but we need to use the carry flag instead of the N.

So if we change the bpl instruction to the BCS, we will get correct results for every unsigned number combination.

The comparison of signed numbers is also possible but not that straightforward. We will cover that in one of a future episodes.

There is even a better way to compare two numbers together. The CMP command does exactly that; it compares a number with the one already held in the accumulator. Under the hood, it uses exactly the same method of subtraction but it only changes the flags and keeps the accumulator unchanged.

``` asm
  lda number_a
  cmp number_b
```

And contrary to the subtraction, the cmp command has equivalent comparison commands CPX and CPY that work with X and Y registers.

Branching instructions along with comparisons allow our programs to make decisions based on relations with arbitrary numbers. In other words, they are powerful building blocks that we can add to our toolset and use for greater good.

See you soon!  
