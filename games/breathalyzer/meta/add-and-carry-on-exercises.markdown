# Add and Carry On - Exercises

#### 1. Adding bits by hand

Add following numbers without using a calculator.

```
  1101 0101 1111 0110 0000 1111
+ 0111 1101 0101 1110 1010 0110
-------------------------------
```

#### 2. Adding Dwords

.dword directive can be used to define 32-bit numbers. 

Modify a program below by implementing a 32-bit addition.
1. Update score by adding monster_kill_score to it.
2. Compare your result with score_after_killing_monster

```
:BasicUpstart2(main)

main:
  // Update score by adding monster_kill_score to it  
  rts

.pc = 2500
score:
  .word $12345678

monster_kill_score:
  .word $01ff02fe

score_after_killing_monster:
  .word $12345678 + $01ff02fe

```
