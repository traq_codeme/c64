:BasicUpstart2(main)

.const TOP_LEFT_CHARACTER = $0400

main:
  lda #'a'
  sta TOP_LEFT_CHARACTER + 0
  
  lda #' '
  sta TOP_LEFT_CHARACTER + 1
  sta TOP_LEFT_CHARACTER + 2
  sta TOP_LEFT_CHARACTER + 3

  lda #'b'
  sta TOP_LEFT_CHARACTER + 4

  lda number_a
  sec
  sbc number_b
  
  beq a_equal_to_b
  bcs a_greater_than_b

a_less_than_b:
  lda #'<'
  sta TOP_LEFT_CHARACTER + 2
  jmp end

a_equal_to_b:
  lda #'='
  sta TOP_LEFT_CHARACTER + 2
  jmp end

a_greater_than_b:
  lda #'>'
  sta TOP_LEFT_CHARACTER + 2
  jmp end

end:
  rts

.pc = 2500
number_a:
  .byte  1
number_b:
  .byte  255

