# Metaprogramming

In the previous episode, we've used macros to remove the duplicated code required for comparisons and additions. Macros are probably the simplest form of the compile-time metaprogramming. They are widely implemented in other assemblers so even if the syntax differs you can translate the program quite easily from the KickAssembler to the ACME or the TurboAssembler.

However, the KickAssembler has one exclusive feature that allows for really powerful compile-time metaprogramming. It has a built-in programming language that can be used to generate assembly. 

Let's see how it works.

Last week we've been working on test cases that ensured that the score was updated properly after killing monsters.

All of those operations were done on bytes. Let's say we want to implement the same functionality using 16-bit values.

First let's copy the data. And append the 16 to the name of each variable, so we know what version we are using. Then we will change the .byte directive to .word and use actual 16-bit values.  

``` asm
score16:
  .word $1122

monster_kill_score16:
  .word $0102

score_after_killing_first_monster16:
  .word $1122 + $0102

score_after_killing_second_monster16:
  .word $1122 + 2 * $0102
```

Now we can copy the test cases. Rename the labels and use appropriate variables. 

For now let's use the old kill_monster macro, but let's change the assertion name to assert_words_equal.

``` asm
player_kills_first_monster16:
  :kill_monster()

score_is_awarded_for_killing_first_monster16:
  :assert_words_equal(score_after_killing_first_monster16, score16)

player_kills_second_monster16:
  :kill_monster()

score_is_awarded_for_killing_second_monster16: 
  :assert_words_equal(score_after_killing_second_monster16, score16)
```

Compilation error tells us we need to define the macro for this assertion.

Let's use the TDD to write it. 

We need three 16-bit variables with different values. If we compare any of them, the test should fail. And we'll check that in a second.

``` asm
word_a:
  .word $1111

word_b:
  .word $1122

word_c:
  .word $2211
```

``` asm
// :assert_words_equal(word_a, word_b) // should fail
// :assert_words_equal(word_b, word_c) // should fail
// :assert_words_equal(word_c, word_a) // should fail
```

But first, let's make sure that the comparison with the same values passes.

``` asm
:assert_words_equal(word_a, word_a)
:assert_words_equal(word_b, word_b)
:assert_words_equal(word_c, word_c)
```

The implementation is quite easy. We just need to take the second byte and compare it in exactly the same way.

``` asm
    lda actual + 1
    cmp expected + 1
    bne tests_fail
```

The test passes, and we can confirm that every other fails. So it seems that the assertion works.

When we uncomment the 16-bit assertions related to monster killing, we can see that tests are failing.

``` asm
player_kills_first_monster16:
  :kill_monster()

score_is_awarded_for_killing_first_monster16:
  :assert_words_equal(score_after_killing_first_monster16, score16)

player_kills_second_monster16:
  :kill_monster()

score_is_awarded_for_killing_second_monster16: 
  :assert_words_equal(score_after_killing_second_monster16, score16)
```

This is expected since the kill_monster macro adds only the least significant byte of the word.

Let's make a 16-bit version of it.

``` asm
.macro kill_monster16() {

}
```

We've done it before. All we need to do is to add the more significant bytes together and make sure that the carry flag is not modified after the previous addition.

We are green, and we finally have the opportunity to make our code cleaner.

In the kill_monster16, we can add 0 to the first part of an addition. It does not change the behavior of the code, but it makes it look almost identical.

KickAssembler's scripting language has a concept of variables. They are similar to constants, but we can change their values using the .eval directive.

``` asm
.var byte = 0

.eval byte = byte + 1
```

They are evaluated during compilation, and we can add them to constants, labels and use inside of expressions.

If we use them in our code, it becomes identical. 

``` asm
    lda score16 + byte
    adc monster_kill_score16 + byte
    sta score16 + byte
```

So we could, for example, extract it to a macro and pass a variable to it. 

``` asm
.macro add_bytes_with_offset(offset, a, b, result) {
    lda a + offset
    adc b + offset
    sta result + offset
}
```

To make it even more robust, we could use a for loop that the KickAssembler's scripting language provides.

``` asm
  .for(var byte = 0; byte < 2; byte = byte + 1) {
    :add_bytes_with_offset(byte, score16, monster_kill_score16, score16) 
  }
```

We're still green. So let's take it up a notch and create a general macro that can add integers regardless of the precision.

Let's call it an add_integer. The first argument will be the precision in bits. Next we will have the operands, and finally the result. Each of them will be a memory address.

``` asm
.macro add_integer(bits, a, b, result) {

}

```

First we need to calculate the number of bytes from bits. 

``` asm
  .var bytes = bits / 8
```

Now let's copy the code from the kill_monster16 macro. Use bytes variable as a loop terminator, and pass the correct arguments.

``` asm
    clc

  .for(var byte = 0; byte < bytes; byte = byte + 1) {
    :add_bytes_with_offset(byte, a, b, result) 
  }
```
Finally, we are able to use the add_integer in both of kill_monster macros.

In the same way, we can implement an assert_integers_equal macro that can be used to compare integers of arbitrary precision. First we calculate bytes from bits again. Then we use the for loop to calculate the byte offset. And finally we compare each byte of the pair of integers and fail the tests if they are not equal.

``` asm
.macro assert_integers_equal(bits, expected, actual) {
  .var bytes = bits / 8

  .for(var byte = 0; byte < bytes; byte++) {
    lda actual + byte
    cmp expected + byte
    bne tests_fail
  }
}
```

Now, we can replace the implementation of our existing assertions with a call to this macro.

And they still work. But there is a tiny part of the code that is duplicated, and it would be great to be able to reuse it somehow.

I'm talking about the bits to bytes conversion. It's only one line of code, but it seems like something that can be useful in multiple different situations.

KickAssembler's scripting language allows us to define a function that can be used to compute a value from given arguments.

Let's make one called bits_to_bytes and reuse it in both macros.

``` asm
.function bits_to_bytes(bits) {
  .return bits / 8
}
```

Compile-time metaprogramming techniques can be really powerful. Today we've seen how to use them to refactor the code, but it's not the only use case. We can use it to precompute data, or even generate highly optimized code for demos or games.

Most other assemblers provide only basic metaprogramming features, forcing us to use external tools or even to write our own to do more sophisticated code transformations.

KickAssembler with it's scripting language is really convenient and allows us to do quite a lot without using any other external tool.

See you soon. 
