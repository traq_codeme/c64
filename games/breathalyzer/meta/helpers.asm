.importonce
.pseudocommand mov source: dest {
  :_mov 1: source: dest
}

.pseudocommand mov16 source: dest {
  :_mov 2: source: dest
}

.pseudocommand _mov bytes: source: dest {
  .for(var byte_id = 0; byte_id < bytes.getValue(); byte_id++) {
    lda extract_byte_argument(source, byte_id)
    sta extract_byte_argument(dest, byte_id)
  }
}

.pseudocommand add a: b: result {
  :_add 1: a: b: result
}

.pseudocommand add16 a: b: result {
  :_add 2: a: b: result
}

.pseudocommand _add bytes: a: b: result {
  .if (result.getType() == AT_NONE) {
    .eval result = a
  } 
    clc 
  .for(var byte_id = 0; byte_id < bytes.getValue(); byte_id++) {
    lda extract_byte_argument(a, byte_id)
    adc extract_byte_argument(b, byte_id)
    sta extract_byte_argument(result, byte_id)
  }
}
.function extract_byte_argument(arg, byte_id) {
  .if (arg.getType() == AT_IMMEDIATE) {
    .return CmdArgument(arg.getType(), extract_byte(arg.getValue(), byte_id))
  }
  .return CmdArgument(arg.getType(), arg.getValue() + byte_id)
}

.function extract_byte(value, byte_id) {
  .var bits = byte_id * 8
  .eval value = value >> bits
  .return value & 255
}
