.import source "helpers.asm"

:BasicUpstart2(main)
.label kernal_chrout = $ffd2
.label screen_memory = $0400

.label joystick1_port = $dc01
.label joystick2_port = $dc00

.const JOY_UP     = %00000001
.const JOY_DOWN   = %00000010
.const JOY_LEFT   = %00000100
.const JOY_RIGHT  = %00001000
.const JOY_BUTTON = %00010000

.const JOY_NEUTRAL = %00011111

main:
  lda #147
  jsr kernal_chrout

game_loop:
  :draw_player()

  lda joystick1_port
  and #JOY_NEUTRAL
  sta joystick_state
  lda joystick_previous_state
  eor #JOY_NEUTRAL
  beq can_handle_joystick
  jmp end_joystick
can_handle_joystick:
  :handle_joystick_input(joystick_state)
end_joystick:
  :mov joystick_state; joystick_previous_state

  jmp game_loop
  
.pc = * "Data"
  player_character: .byte 'p'
  player_position: .word screen_memory + 500
  player_previous_position: .word screen_memory

  joystick_state: .byte 0
  joystick_previous_state: .byte JOY_NEUTRAL

.macro handle_joystick_input(joystick) {
  lda joystick 
  and #JOY_BUTTON
  bne !no_action+
!action:
  :mov #'a'; player_character
  jmp end
!no_action:
  lda joystick 
  and #JOY_UP
  bne !no_action+
!action:
  :mov #'u'; player_character
  :add16 player_position; #-40
  jmp end
!no_action:
  lda joystick 
  and #JOY_DOWN
  bne !no_action+
!action:
  :mov #'d'; player_character
  :add16 player_position; #40
  jmp end
!no_action:
  lda joystick 
  and #JOY_LEFT
  bne !no_action+
!action:
  :mov #'l'; player_character
  :add16 player_position; #-1
  jmp end
!no_action:
  lda joystick 
  and #JOY_RIGHT
  bne !no_action+
!action:
  :mov #'r'; player_character
  :add16 player_position; #1
  jmp end
!no_action:
end:
}

.macro draw_player() {

  :mov16 player_previous_position; previous_position
  lda #' '
.label previous_position = * + 1
  sta screen_memory + 500

  :mov16 player_position; position
  lda player_character
.label position = * + 1
  sta screen_memory + 500

  :mov16 player_position; player_previous_position
}


