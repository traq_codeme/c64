:BasicUpstart2(main)

main:
  clc
  lda experience 
  adc monster_kill_exp
  sta experience
  rts

.pc = 2500
experience:
  .byte 100
monster_kill_exp:
  .byte 10
