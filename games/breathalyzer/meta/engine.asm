.const SP0X        = $D000;
.const SP0Y        = $D001;
.const SP1X        = $D002;
.const SP1Y        = $D003;
.const SP2X        = $D004;
.const SP2Y        = $D005;
.const SP3X        = $D006;
.const SP3Y        = $D007;
.const SP4X        = $D008;
.const SP4Y        = $D009;
.const SP5X        = $D00A;
.const SP5Y        = $D00B;
.const SP6X        = $D00C;
.const SP6Y        = $D00D;
.const SP7X        = $D00E;
.const SP7Y        = $D00F;
.const MSIGX       = $D010;
.const VICCR1      = $D011;       //  8ebd ryyy
                                //        ^^^   - Y scroll
                                //       ^      - RSEL:     Row count           (0:24, 1:25)
                                //     ^        - DEN:      Display enable      (1:enabled)
                                //    ^         - BMM:      Bitmap mode
                                //   ^          - ECM:      Extended Color Mode
                                //  ^           - RST8:     9th bit of raster interrupt position

.const SCROLY      = $D011;       // alias
.const RASTER      = $D012;
.const LPENX       = $D013;
.const LPENY       = $D014;
.const SPENA       = $D015;
.const VICCR2      = $D016;       // --rm cxxx
                                //       ^^^    - X scroll
                                //      ^       - CSEL:     Column count        (0:38, 1:40)
                                //    ^         - MCM:      Multicolor mode
                                //   ^          - RES:      Reset

.const SCROLX      = $D016;       // alias
.const YXPAND      = $D017;       // Sprite double height register
.const VMCSB       = $D018;       // ssss ccc-    s: Screen pointer (A13-A10),    c: Bitmap/charset pointer (A13-A11)
.const VICIRQ      = $D019;
.const IRQMASK     = $D01A;
.const SPBGPR      = $D01B;
.const SPMC        = $D01C;       // Sprite multicolor mode register
.const XXPAND      = $D01D;       // Sprite double width register
.const SPSPCL      = $D01E;
.const SPBGCL      = $D01F;
.const EXTCOL      = $D020;
.const BGCOL0      = $D021;
.const BGCOL1      = $D022;
.const BGCOL2      = $D023;
.const BGCOL3      = $D024;
.const SPMC0       = $D025;       // Sprite extra color #1
.const SPMC1       = $D026;       // Sprite extra color #2
.const SP0COL      = $D027;
.const SP1COL      = $D028;
.const SP2COL      = $D029;
.const SP3COL      = $D02A;
.const SP4COL      = $D02B;
.const SP5COL      = $D02C;
.const SP6COL      = $D02D;
.const SP7COL      = $D02E;


        .var music = LoadSid("test01.sid")
        BasicUpstart2(start)
start:
 
        ldx #0
        ldy #0
        lda #music.startSong-1
        jsr music.init
        sei
        lda #<irq1
        sta $0314
        lda #>irq1
        sta $0315
        lda #$7         // 0---00111
        sta $dc0d       // disable timer interrupts
        lda #$81        // 10000001
        sta $d01a       // enable rater interrupts
        lda #$1b        // 0 <- MSB to Write: Raster line to generate interrupt at (bit #8). and rest 0011 011
        sta $d011       // screen control register
        lda #$80        // dec 128
        sta $d012       // Raster line to generate interrupt at  (bits #0-#7).
        cli             // Enable interrupts

// grapic load
        lda $dd00       
        and #$fc        //zeruje dwa najmłodsze bity
        ora #1        
        sta $dd00
        //powższe cztery linie przenosi pamięc VIC II w trzecią ćwiartkę od 8000 do BFFF

// petla kopiuje 256 bajtów ($100) zalodowanego sprite do pamieci zaczynającej się od $8000
        ldx #0
!loop:
        .for (var i=0; i<8; i++) {
           lda sprite_import+i*$100,x
           sta $8000+i*$100,x
        }
        
        inx  // jeżeli x bedzie 255  to inx inkrementuje o 1 i przekreca licznik na 0
        bne !loop- // Ma skoczyc jezeli flaga Z = 0 (gdy x się nie przekręcił) do najbliższej poprzedzającej !loop

// sprites
        lda #0
        sta $8400+1024-8

        lda #100
        sta SP0X
        sta SP0Y
        lda #50
        sta SP1X
        sta SP1Y
        lda #%00000011 // włączenie sprite
        sta SPENA
        lda #1
        sta SPMC
        lda #1
        sta SPMC0
        sta SPMC1



        jmp * // nieskoczona petla

//---------------------------------------------------------
// interrupt of sreen lines drawing
irq1:
        //asl $d019
        lda #1
        sta $d019
        //jsr music.play
     
        //animacja

        //clc
        //lda SP0X
        //adc #5
        //sta SP0X
        //clc
        //lda SP0Y
        //adc #5
        //sta SP0Y
        //inc SP0X
        //inc SP0Y

        // end of interrupts
        pla // get accumylator from the stack
        tay // transfer accumulator to Y
        pla // get accumylator from the stack
        tax // transfer accumulator to X
        pla // get accumylator from the stack
        rti // return from iterrupt
//---------------------------------------------------------
        *=music.location "Music"
        .fill music.size, music.getData(i)

// import sprite
sprite_import:
        .import binary "keyhole.raw", 0, 128
        