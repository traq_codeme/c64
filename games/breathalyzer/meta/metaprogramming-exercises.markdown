# Macros - Exercises

#### 1. DWORD subtraction and comparison

In the exercises below follow the "// TODO:" comments

a) Create an assert_dwords_equal macro and test it

b) Create a general subtract_integers macro and test if it forks for dwords

c) Create an assert_dwords_not_equal macro and test it. To make this more challenging try to generalize it into the assert_integers_not_equal macro.


``` asm
:BasicUpstart2(main)

.const BORDER = $d020

main:

dword_equality_assertion_works:
  :assert_dwords_equal(dword_a, dword_a)
  :assert_dwords_equal(dword_b, dword_b)
  :assert_dwords_equal(dword_c, dword_c)
  :assert_dwords_equal(dword_d, dword_d)
  :assert_dwords_equal(dword_e, dword_e)

// TODO: a) uncoment temporarily asserts below (one at the time) to check if the assert fails when it should
//  :assert_dwords_equal(dword_a, dword_b) // this should fail
//  :assert_dwords_equal(dword_a, dword_c) // this should fail
//  :assert_dwords_equal(dword_a, dword_d) // this should fail
//  :assert_dwords_equal(dword_a, dword_e) // this should fail

dword_subtraction_works:
  clc
  :subtract_dwords(number_a, number_b, result)
// TODO: b) uncomment asserts below if all previous tests are green
//  :assert_dwords_equal(expected_result, result)


dword_inequality_assertion_works:
// TODO: c) uncomment asserts below if the tests are green
// :assert_dwords_not_equal(dword_a, dword_b)
// :assert_dwords_not_equal(dword_a, dword_c)
// :assert_dwords_not_equal(dword_a, dword_d)
// :assert_dwords_not_equal(dword_a, dword_e)

// TODO: c) uncoment temporarily asserts below (one at the time) to check if the assert fails when it should
//  :assert_dwords_not_equal(dword_a, dword_a) // this should fail
//  :assert_dwords_not_equal(dword_b, dword_b) // this should fail
//  :assert_dwords_not_equal(dword_c, dword_c) // this should fail
//  :assert_dwords_not_equal(dword_d, dword_d) // this should fail
//  :assert_dwords_not_equal(dword_e, dword_e) // this should fail



render_test_result:
  lda test_result
  sta BORDER
  rts

tests_fail:
  lda #RED
  sta test_result
  jmp render_test_result

test_result:
  .byte GREEN


number_a:
  .dword $11223344

number_b:
  .dword $01020304

result:
  .dword $0

expected_result:
  .dword $11223344 - $01020304

dword_a:
  .dword $11111111
dword_b:
  .dword $00111111
dword_c:
  .dword $11001111
dword_d:
  .dword $11110011
dword_e:
  .dword $11111100

.macro assert_dwords_equal(expected, actual) {
    // TODO: a). implement this assertion
    jmp tests_fail
}


.macro subtract_dwords(a, b, result) {
    :subtract_integers(32, a, b, result)
}

.macro subtract_integers(bits, a, b, result) {
    // TODO: b) implement this macro
}

.macro assert_dwords_not_equal(expected, actual) {
  // TODO: c) implement this macro
  fail:
    jmp tests_fail
  pass:
}

.macro assert_integers_equal(bits, expected, actual) {
  .var bytes = bits_to_bytes(bits)

  .for(var byte = 0; byte < bytes; byte++) {
    lda actual + byte
    cmp expected + byte
    bne fail
  }
    jmp pass
  fail:
    jmp tests_fail
  pass:
}

.function bits_to_bytes(bits) {
  .return bits / 8
}

```
