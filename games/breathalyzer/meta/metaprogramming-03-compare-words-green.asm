:BasicUpstart2(main)

.const BORDER = $d020

main:

player_kills_first_monster:
  :kill_monster()

score_is_awarded_for_killing_first_monster:
  :assert_bytes_equal(score_after_killing_first_monster, score)

player_kills_second_monster:
  :kill_monster()

score_is_awarded_for_killing_second_monster: 
  :assert_bytes_equal(score_after_killing_second_monster, score)



player_kills_first_monster16:
  :kill_monster()

score_is_awarded_for_killing_first_monster16:
  //:assert_words_equal(score_after_killing_first_monster16, score16)

player_kills_second_monster16:
  :kill_monster()

score_is_awarded_for_killing_second_monster16: 
  //:assert_words_equal(score_after_killing_second_monster16, score16)


:assert_words_equal(word_a, word_a)
:assert_words_equal(word_b, word_b)
:assert_words_equal(word_c, word_c)

// :assert_words_equal(word_a, word_b) // should fail
// :assert_words_equal(word_b, word_c) // should fail
// :assert_words_equal(word_c, word_a) // should fail

render_test_result:
  lda test_result
  sta BORDER
  rts

// subroutines
tests_fail:
  lda #RED
  sta test_result
  jmp render_test_result

.pc = 2500 "Variables"
score:
  .byte $11

monster_kill_score:
  .byte $01

score_after_killing_first_monster:
  .byte $11 + $01

score_after_killing_second_monster:
  .byte $11 + 2 * $01

score16:
  .word $1122

monster_kill_score16:
  .word $0102

score_after_killing_first_monster16:
  .word $1122 + $0102

score_after_killing_second_monster16:
  .word $1122 + 2 * $0102


word_a:
  .word $1111

word_b:
  .word $1122

word_c:
  .word $2211

test_result:
  .byte GREEN


.macro kill_monster() {
    clc
    lda score
    adc monster_kill_score
    sta score
}

.macro assert_bytes_equal(expected, actual) {
    lda actual 
    cmp expected
    bne tests_fail
}

.macro assert_words_equal(expected, actual) {
    lda actual 
    cmp expected
    bne tests_fail
    lda actual + 1
    cmp expected + 1
    bne tests_fail
}
