:BasicUpstart2(main)

.const TOP_LEFT_CHARACTER = $0400

main:
  lda #'a'
  sta TOP_LEFT_CHARACTER + 0
  
  lda #' '
  sta TOP_LEFT_CHARACTER + 1
  sta TOP_LEFT_CHARACTER + 2
  sta TOP_LEFT_CHARACTER + 3

  lda #'0'
  sta TOP_LEFT_CHARACTER + 4

  lda number_a

  bmi a_less_than_0
  bne a_greater_than_0
  jmp a_equal_to_0

a_less_than_0:
  lda #'<'
  sta TOP_LEFT_CHARACTER + 2
  jmp end

a_equal_to_0:
  lda #'='
  sta TOP_LEFT_CHARACTER + 2
  jmp end

a_greater_than_0:
  lda #'>'
  sta TOP_LEFT_CHARACTER + 2
  jmp end

end:
  rts

.pc = 2500
number_a:
  .byte  -5

