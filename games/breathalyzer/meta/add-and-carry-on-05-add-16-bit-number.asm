:BasicUpstart2(main)

main:
  clc
  lda score 
  adc monster_kill_score
  sta score
  lda score + 1
  adc monster_kill_score + 1
  sta score + 1
  rts

.pc = 2500
score:
  .word $1234

monster_kill_score:
  .word $01ff

score_after_killing_monster:
  .word $1234 + $01ff
