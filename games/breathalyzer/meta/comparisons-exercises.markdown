# Comparisons - Exercises


#### 1. Word comparison with subtraction

Write comparison procedure that will use subtraction to compare two unsigned 16-bit variables: number_a and number_b

``` asm
:BasicUpstart2(main)

.const BORDER = $d020
.const LESS_CHAR = '<'
.const GREATER_CHAR = '>'
.const EQUAL_CHAR = '='

.const SCREEN = $0400

main:
  lda #'a'
  sta SCREEN + 0
  
  lda #' '
  sta SCREEN + 1
  sta SCREEN + 2
  sta SCREEN + 3

  lda #'b'
  sta SCREEN + 4

// TODO: Compare numbers here

a_less_than_b:
  lda #'<'
  sta SCREEN + 2
  jmp end

a_equal_to_b:
  lda #'='
  sta SCREEN + 2
  jmp end


a_greater_than_b:
  lda #'>'
  sta SCREEN + 2
  jmp end

end:
  rts

.pc = 2500
number_a:
  .word  1
number_b:
  .word  $ffff
```

#### 2. Word comparison with cmp, cpy or cpx

Write comparison procedure that will that will use comparison commands (CMP, CPX, CPY) to compare two 16-bit variables: number_a and number_b

``` asm
:BasicUpstart2(main)

.const BORDER = $d020
.const LESS_CHAR = '<'
.const GREATER_CHAR = '>'
.const EQUAL_CHAR = '='

.const SCREEN = $0400

main:
  lda #'a'
  sta SCREEN + 0
  
  lda #' '
  sta SCREEN + 1
  sta SCREEN + 2
  sta SCREEN + 3

  lda #'b'
  sta SCREEN + 4

// TODO: Compare numbers here

a_less_than_b:
  lda #'<'
  sta SCREEN + 2
  jmp end

a_equal_to_b:
  lda #'='
  sta SCREEN + 2
  jmp end


a_greater_than_b:
  lda #'>'
  sta SCREEN + 2
  jmp end

end:
  rts

.pc = 2500
number_a:
  .word  1
number_b:
  .word  $ffff
```

