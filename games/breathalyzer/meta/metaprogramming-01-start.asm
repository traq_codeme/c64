:BasicUpstart2(main)

.const BORDER = $d020

main:

player_kills_first_monster:
  :kill_monster()

score_is_awarded_for_killing_first_monster:
  :assert_bytes_equal(score_after_killing_first_monster, score)

player_kills_second_monster:
  :kill_monster()

score_is_awarded_for_killing_second_monster: 
  :assert_bytes_equal(score_after_killing_second_monster, score)

render_test_result:
  lda test_result
  sta BORDER
  rts

// subroutines
tests_fail:
  lda #RED
  sta test_result
  jmp render_test_result

.pc = 2500 "Variables"
score:
  .byte $11

monster_kill_score:
  .byte $01

score_after_killing_first_monster:
  .byte $11 + $01

score_after_killing_second_monster:
  .byte $11 + 2 * $01

test_result:
  .byte GREEN


.macro kill_monster() {
    clc
    lda score
    adc monster_kill_score
    sta score
}

.macro assert_bytes_equal(expected, actual) {
    lda actual 
    cmp expected
    bne tests_fail
}
