.importonce

// wczytanie poczatkowej grafiki
.const colorRam = $1c00 + $8000
.const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"
.var picture = LoadBinary("gfx/alkomat.kla", KOALA_TEMPLATE)

//wczytanie muzyki 
.var music = LoadSid("msx/test01.sid")
*=music.location "Music load"
.fill music.size, music.getData(i)

//$0c00 + $8000 "Screen ram"; 
.pc = * "Image_load"
image_screen:
.fill picture.getScreenRamSize(), picture.getScreenRam(i) 
//*=$1c00 + $8000 "Color ram" ; 
image_color:
.fill picture.getColorRamSize(), picture.getColorRam(i) 
//*=$2000 + $8000 "Bitmap ram"; 
image_bitmap:
.fill picture.getBitmapSize(), picture.getBitmap(i) 

//wczytanie spritów 
.pc = * "Sprites load"
sprites_data:
	.import binary "gfx/keyhole.bin"
