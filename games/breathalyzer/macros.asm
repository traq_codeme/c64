.importonce

.macro animacje_dziurki() {
    
    lda score
    cmp #0
    bne !nextdziurka+
    jsr anim_hole4
  
  !nextdziurka:
    lda score
    cmp #1
    bne !nextdziurka+
    jsr anim_hole2

  !nextdziurka:
    lda score
    cmp #2
    bne !nextdziurka+
    jsr anim_hole3

  !nextdziurka:
    lda score
    cmp #3
    bne !nextdziurka+
    jsr anim_hole1
  
  !nextdziurka:           
}


.macro joy(joystick, button) {
// joystick (0 = port2, 1 = port1 )
// button 0-5
  lda joyport+joystick
  and #1<<button
 // ustawia Z
}

.macro joyone(joystick, button) {
// joystick (0 = port2, 1 = port1 )
// button 0-5
  lda joylast+joystick
  eor #$ff
  ora joyport+joystick
  and #1<<button //maska
  php //stan flag na stos
  lda joyport+joystick
  // eor #ff (byc moze)
  sta joylast+joystick
  plp //stan flag ze stosu
// ustawia Z
}

.macro handle_joystick_input() {
  :joyone(0,4) //FIRE
  beq !action+
  jmp !no_action+
!action:
  :write_text(catch_hole,screen_memory +0,0)
  :collisions(sprites.klucz_x, sprites.klucz_y, sprites.dziurka_x, sprites.dziurka_y)  
  ldx tries
  dex
  stx tries
  bne !continue+ // tries > 0 to skok do live
  :score_display()
  :write_text(end_txt,screen_memory +1,0)  
  :game_restart()
  
!continue:
  :score_display()
  lda score
  cmp #4
  bcc !no_action+
  :score_display()
  :write_text(win_txt,screen_memory +1,0)
  :game_restart()
   

!no_action:
  :joy(0,0) //UP
  bne !no_action+
!action:
  lda sprites.klucz_y
  cmp #60
  bcc !no_action+
  dec sprites.klucz_y

!no_action:
  joy(0,1) //DOWN
  bne !no_action+
!action:
  lda sprites.klucz_y
  cmp #224
  bcs !no_action+
  inc sprites.klucz_y 
 
!no_action:
  joy(0,2) //LEFT
  bne !no_action+
!action:
  lda sprites.klucz_x
  cmp #24
  bcc !no_action+
  dec sprites.klucz_x
 
!no_action:
  joy(0,3)  //RIGHT
  bne !no_action+
!action:
  lda sprites.klucz_x
  cmp #254
  bcs !no_action+
  inc sprites.klucz_x
 
!no_action:
  ldx #DARK_GREY
  //ldx #BLACK
  lda joystick2_port
  eor #$ff
  and #JOY_NEUTRAL
  beq no_joy
  ldx #BLACK
no_joy:
  stx sprites.colors
end:
}

.macro sprites_preaparation(){
  copy_pages(sprites_data, 250*64+$8000, 1)
  lda #%00110100
  sta $d018

    //włączenie sprite
  lda #%00000011
  sta sprites.enable_bits
 
//poczatkowa pozycja klucza
  lda #25
  //sta sprites.positions + 2 // oś x
  sta sprites.klucz_x
  lda #60
  //sta sprites.positions + 3 //oś y
  sta sprites.klucz_y

  // który sprite
  lda #250
  sta sprites.pointers + 0

  lda #251
  sta sprites.pointers + 1

  // powiekszenie sprite
  lda #%0000000
  sta sprites.vertical_stretch_bits

  lda #%00000000
  sta sprites.horizontal_stretch_bits
  
  lda #%00000000
  sta sprites.multicolor_bits

  //lda #DARK_GREY
  lda #BLACK 
  //sprite 1
  sta sprites.colors

  lda #YELLOW
  //sprite 2
  sta sprites.colors + 1

}

.macro clear_screen() {

    ldx #0
    lda #' '
  loopy:
    .for (var i = 0; i < 25; i++) {
    sta screen_memory + 40*i, X
    }
    inx
    cpx #32
    bne loopy

    ldx #32
    lda #' ' +64
  loopz:
    .for (var i = 0; i < 25; i++) {
    sta screen_memory + 40*i, X
    }
    inx
    cpx #40
    bne loopz


   ldx #0
   lda #5 // kolor zanków na polu gry
  loopy1:
    .for (var i = 0; i < 25; i++) {
    sta $d800 + 40*i, X
    }
    inx
    cpx #32
    bne loopy1

    ldx #32
    lda #WHITE
  loopz1:
    .for (var i = 0; i < 25; i++) {
    sta $d800 + 40*i, X
    }
    inx
    cpx #40
    bne loopz1

  lda #DARK_GREY
  sta background_color_reg
  lda #BLACK
  sta ext1_background_color_reg
  lda #GREY
  sta border_color_reg



}

// score display
.macro score_display(){
  // ########
  // score nn
  :write_text(score_txt,screen_memory +32,1)
    lda #'0'+64
    clc
    adc score
    sta screen_memory + 32 +7
  // ########
  // tries nn 
  :write_text(tries_txt,screen_memory +40 +32,1)
    lda #'0'+64
    clc
    adc tries
    sta screen_memory + 40 + 32 +7
}

.macro music_play(){
  jsr music.play
}

.macro music_stop(){
  jsr music.stop
}

.macro music_init (music_nr) {
  ldx #0
  ldy #0
  lda #music_nr
  jsr music.init
}

.macro copy_pages(src, dst, pages){
  .for (var i=0; i<pages; i++) {
    ldx #0
    copy:
      lda src+i*$100,x
      sta dst+i*$100,x
      inx
      bne copy
  }
} 

.macro write_text(text, dst, bg_color){
    ldx #0
  !loop:
    lda text, X
    cmp #0
    beq end
    and #%00111111
    ora #bg_color * 64
    sta dst, X
    inx
    jmp !loop-
   end:
}

.macro game_restart() {
    lda #9
    sta tries
    lda #0
    sta score
}