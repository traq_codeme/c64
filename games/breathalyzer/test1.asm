:BasicUpstart2(main)

//.label joystick1_port = $dc01
//.label joystick2_port = $dc00

.label joyport = $dc00

.const JOY_UP     = %00000001
.const JOY_DOWN   = %00000010
.const JOY_LEFT   = %00000100
.const JOY_RIGHT  = %00001000
.const JOY_BUTTON = %00010000
.const JOY_NEUTRAL = %00011111
.label border_color_reg = $d020

joylast: .byte 0

main:

  :joydown(0, 4)
  beq black // jak fire to back czyli Z == 1
green:
  lda #GREEN
  sta border_color_reg
  //jmp end
black:
  lda #BLACK
  sta border_color_reg
end:
  jmp main


.macro joy(joystick, button) {
// joystick (0 = port2, 1 = port1 )
// button 0-5
  lda joyport+joystick
  and #1<<button
 // ustawia Z
}

.macro joydown(joystick, button) {
// joystick (0 = port2, 1 = port1 )
// button 0-5
  lda joyport+joystick
  //eor #ff (byc moze)
  and joylast+joystick
  and #1<<button 
  php
  lda joyport+joystick
  // eor #ff (byc moze)
  sta joylast+joystick
  plp
// ustawia Z
}
