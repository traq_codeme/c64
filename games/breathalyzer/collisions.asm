.const COLLISIONX1 = $02 //(or wherever you want it)
.const COLLISIONX2 = $03
.const COLLISIONY1 = $04
.const COLLISIONY2 = $05

.const XSIZE1 = $06 //The area of the drawn sprite on the left
.const XSIZE2 = $0C //The area of the drawn sprite on the right
.const YSIZE1 = $0C //The area of the drawn sprite at the top
.const YSIZE2 = $18 //The area of the drawn sprite at the bottom

.macro collisions(sprites_klucz_x, sprites_klucz_y, sprites_dziurka_x, sprites_dziurka_y){
	//calculate the collision routine
	lda sprites_klucz_x
	sec
	sbc #XSIZE1
	sta COLLISIONX1
	clc
	adc #XSIZE2
	sta COLLISIONX2
	lda sprites_klucz_y
	sec
	sbc #YSIZE1
	sta COLLISIONY1
	clc
	adc #YSIZE2
	sta COLLISIONY2

	//main collision detection
	lda sprites_dziurka_x
	cmp COLLISIONX1
	bcc NOTHIT
	cmp COLLISIONX2
	bcs NOTHIT
	lda sprites_dziurka_y
	cmp COLLISIONY1
	bcc NOTHIT
	cmp COLLISIONY2
	bcs NOTHIT
	HIT:
	    inc score
	NOTHIT:
}