        .const colorRam = $1c00 + $8000
        .const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"
        .var picture = LoadBinary("abc.kla", KOALA_TEMPLATE)
        .var music = LoadSid("sids/flimbo.sid")
        BasicUpstart2(start)
start:
 
        ldx #0
        ldy #0
        lda #music.startSong-1
        jsr music.init
        sei
        lda #<irq1
        sta $0314
        lda #>irq1
        sta $0315
        lda #$7         // 0---00111
        sta $dc0d       // disable timer interrupts
        lda #$81        // 10000001
        sta $d01a       // enable rater interrupts
        lda #$1b        // 0 <- MSB to Write: Raster line to generate interrupt at (bit #8). and rest 0011 011
        sta $d011       // screen control register
        lda #$80        // dec 128
        sta $d012       // Raster line to generate interrupt at  (bits #0-#7).
        cli             // Enable interrupts

// grapic load
        lda $dd00       
        and #$fc        //zeruje dwa najmłodsze bity
        ora #1        
        sta $dd00
        //powższe cztery linie przenosi pamięc VIC II w trzecią ćwiartkę od 8000 do BFFF
        lda #$38
        sta $d018
        lda #$d8
        sta $d016
        lda #$3b
        sta $d011
        lda #0
        sta $d020
        lda #picture.getBackgroundColor()
        sta $d021
        ldx #0
!loop:
        .for (var i=0; i<4; i++) {
           lda colorRam+i*$100,x
           sta $d800+i*$100,x
        }
        inx
        bne !loop-
        jmp *

//---------------------------------------------------------
irq1:
        //asl $d019
        lda #1
        sta $d019
        jsr music.play 
        pla // get accumylator from the stack
        tay // transfer accumulator to Y
        pla // get accumylator from the stack
        tax // transfer accumulator to X
        pla // get accumylator from the stack
        rti // return from iterrupt
//---------------------------------------------------------
        *=music.location "Music"
        .fill music.size, music.getData(i)


*=$0c00 + $8000;      .fill picture.getScreenRamSize(), picture.getScreenRam(i)
*=$1c00 + $8000;      .fill picture.getColorRamSize(), picture.getColorRam(i)
*=$2000 + $8000;      .fill picture.getBitmapSize(), picture.getBitmap(i)
