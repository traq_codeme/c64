        .const colorRam = $1c00 + $8000
        .var music = LoadSid("sids/flimbo.sid")
        BasicUpstart2(start)
start:
        lda #fname_end-fname
        ldx #<fname
        ldy #>fname
        jsr load_img

        ldx #0
        ldy #0
        lda #music.startSong-1
        jsr music.init
        sei
        lda #<irq1
        sta $0314
        lda #>irq1
        sta $0315
        lda #$7         // 0---00111
        sta $dc0d       // disable timer interrupts
        lda #$81        // 10000001
        sta $d01a       // enable rater interrupts
        //lda #$1b        // 0 <- MSB to Write: Raster line to generate interrupt at (bit #8). and rest 0011 011
        //sta $d011       // screen control register
        lda #$80        // dec 128
        sta $d012       // Raster line to generate interrupt at  (bits #0-#7).
        cli             // Enable interrupts

        jmp *

//---------------------------------------------------------
irq1:
        //asl $d019
        lda #1
        sta $d019
        jsr music.play 
        pla // get accumylator from the stack
        tay // transfer accumulator to Y
        pla // get accumylator from the stack
        tax // transfer accumulator to X
        pla // get accumylator from the stack
        rti // return from iterrupt
//---------------------------------------------------------
        *=music.location "Music"
        .fill music.size, music.getData(i)

//---------------- loading file --------------

loadfile:
        jsr $FFBD     // call SETNAM
        lda #$01
        ldx $BA       // last used device number
        bne skip
        ldx #$08      // default to device 8
skip:

        ldy #$01     // not $01 means: load to address stored in file
        jsr $FFBA     // call SETLFS

        lda #$00      // $00 means: load to memory (not verify)
        jsr $FFD5     // call LOAD
        bcs error    // if carry set, a load error has happened
        rts
error:
        // Accumulator contains BASIC error code
//       // most likely errors:
//       // A = $05 (DEVICE NOT PRESENT)
//       // A = $04 (FILE NOT FOUND)
//       // A = $1D (LOAD ERROR)
        // A = $00 (BREAK, RUN/STOP has been pressed during loading)

        rts
//-----------------------------------------------------------------
        .const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"
        .var picture = LoadBinary("abc.kla", KOALA_TEMPLATE)

load_img:
        
        jsr loadfile
        lda $dd00       
        and #$fc        //zeruje dwa najmłodsze bity
        ora #0        
        sta $dd00
        //powższe cztery linie przenosi pamięc VIC II w trzecią ćwiartkę od 8000 do BFFF
        lda #$38        // 0110 100 0
        sta $d018
        lda #$d8
        sta $d016
        lda #$3b
        sta $d011
        lda #0
        sta $d020
        lda #picture.getBackgroundColor()
        sta $d021
        ldx #0
!loop:
        .for (var i=0; i<4; i++) {
           lda colorRam+i*$100,x
           sta $d800+i*$100,x
        }
        inx
        bne !loop-

        rts


fname:  .text "PIC"
fname_end:
