        .const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"
        .var picture = LoadBinary("abc.kla", KOALA_TEMPLATE)

*=$0c00 + $8000;      .fill picture.getScreenRamSize(), picture.getScreenRam(i)
*=$1c00 + $8000;      .fill picture.getColorRamSize(), picture.getColorRam(i)
*=$2000 + $8000;      .fill picture.getBitmapSize(), picture.getBitmap(i)
